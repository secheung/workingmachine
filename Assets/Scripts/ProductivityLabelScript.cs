﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ProductivityLabelScript : MonoBehaviour
{
	ProductivityManagerScript _productManager;
	[SerializeField]
	TextMeshProUGUI text;

	// Start is called before the first frame update
	void Start()
    {
		_productManager = GetComponent<ProductivityManagerScript>();
		_productManager.NextProductivityGoalEvent += OnProductivityDone;
	}

	private void OnProductivityDone()
	{
		text.text = ProductivityManagerScript._score.ToString();
	}
}
