﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarUIScript : MonoBehaviour
{
	RectTransform _barRect = null;
	float _initWidth = 340.0f;

	[Range(0f,1f)]
	[SerializeField]
	protected float _normalizedVal = 1.0f;
	public float NormalizedVal {
		get { return _normalizedVal; }
		set { _normalizedVal = value; }
	}

	// Start is called before the first frame update
	void Start()
    {
		GameObject bar = transform.Find("Bar").gameObject;
		_barRect = bar.GetComponent<RectTransform>();
		if(bar != null) {
			_initWidth = _barRect.rect.width;
		}
	}

    // Update is called once per frame
    void Update()
    {
		_barRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _normalizedVal * _initWidth);
	}

	public void UpdateView(float num, float dem)
	{
		_normalizedVal = Mathf.Min(num/dem, 1.0f);
	}
}
