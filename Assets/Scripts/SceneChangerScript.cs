﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangerScript : MonoBehaviour
{
	[SerializeField]
	ProductivityTimerScript _timerScript;

	// Start is called before the first frame update
	void Start()
    {
		_timerScript.TimeUpEvent += OnTimeUp;

	}

	void OnTimeUp()
	{
		SceneManager.LoadScene("GameOverScene");
	}
}
