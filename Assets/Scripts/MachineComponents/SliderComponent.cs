﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderComponent : MachineComponent
{
	[SerializeField]
	GameObject _ball;

	[SerializeField]
	Transform _endPtL;
	[SerializeField]
	Transform _endPtR;
	[SerializeField]
	Transform _windowPtL;
	[SerializeField]
	Transform _windowPtR;

	bool _movingLtoR;
	float _interpT = 0.0f;
	[SerializeField]
	float _moveSpeed = 1.0f;

	[SerializeField]
	private float _workRate = 1.0f;

	protected KeyCode _keyL;
	protected KeyCode _keyR;

	// Awake is called before the first frame update
	protected override void Awake()
    {
		base.Awake();

		//_ball = GameObject.Find("ball").gameObject;
		//
		//_endPtL    = GameObject.Find("end_pt.L").transform;
		//_endPtR    = GameObject.Find("end_pt.R").transform;
		//_windowPtL = GameObject.Find("window_pt.L").transform;
		//_windowPtR = GameObject.Find("window_pt.R").transform;

		_movingLtoR = false;
	}

	public override void SetupKeys()
	{
		_keyL = InputKeyManager.Instance.getKey();
		_keyR = InputKeyManager.Instance.getKey();
		ControlKeyScript[] ctrlKey = gameObject.GetComponentsInChildren<ControlKeyScript>();
		
		if(ctrlKey[0].transform.position.x < ctrlKey[1].transform.position.x) {
			ctrlKey[0].Key = _keyL;
			ctrlKey[1].Key = _keyR;
		} else {
			ctrlKey[0].Key = _keyR;
			ctrlKey[1].Key = _keyL;
		}
	}

	// Update is called once per frame
	protected override void Update()
    {
		Vector3 startPos = _endPtL.position;
		Vector3 endPos = _endPtR.position;

		Vector3 line = endPos - startPos;

		if (Input.GetKeyDown(_keyL)) {
			_movingLtoR = true;
		} else if (Input.GetKeyDown(_keyR)) {
			_movingLtoR = false;
		}

		_interpT += (_movingLtoR ? _moveSpeed : -_moveSpeed) * Time.deltaTime;
		_interpT = Mathf.Clamp01(_interpT);
		_ball.transform.position = Vector3.Lerp(startPos, endPos, _interpT);
		if (Working())
		{
			Work += _workRate * Time.deltaTime;
		}

		base.Update();
	}

	public override void Reset()
	{
		_interpT = 0;
		Work = 0;
	}

	public override bool Working()
	{
		Vector3 ballPos = _ball.transform.position;
		float dist = Vector3.Distance(_endPtL.position, _endPtR.position);
		float t1 = Vector3.Distance(_windowPtL.position, _endPtL.position) / dist;
		float t2 = Vector3.Distance(_windowPtR.position, _endPtL.position) / dist;

		return (_interpT >= t1 && _interpT <= t2);
	}
}
