﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnnoyingSwitchComponent : SwitchComponent
{
	float _currentTime = 0.0f;
	[SerializeField]
	float _timeDelay = 1.0f;

	// Start is called before the first frame update
	protected override void Awake()
	{
		base.Awake();
		_flipped = false;
	}

	// Update is called once per frame
	protected override void Update()
	{
		if (_flipped)
		{
			if (_currentTime <= 0.0f)
			{
				_currentTime = _timeDelay;
				float coinFlip = Random.Range(0.0f, 1.0f);
				if (coinFlip > 0.5)
				{
					_flipped = false;
				}
			}
		}

		_currentTime -= Time.deltaTime;
		_currentTime = Mathf.Max(0, _currentTime);
		base.Update();
	}

	protected override void CheckWorkDone()
	{
		if (Work >= _workGoal)
		{
			Work = 0;
			DoneWorkEvent(this, _productivity);
		}
	}

	protected override void IncrementWork()
	{
		Work = Work + _workInc * Time.deltaTime;
	}

	public override bool Working()
	{
		return _flipped;
	}
}
