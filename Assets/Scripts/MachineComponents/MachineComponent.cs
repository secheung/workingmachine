﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MachineComponent : MonoBehaviour
{
	public EventHandler<float> DoneWorkEvent = delegate { };

	[SerializeField]
	protected float _productivity = 1.0f;

	[SerializeField]
	protected float _workGoal = 1.0f;

	private BoxCollider2D _collider;
	public BoxCollider2D Collider
	{
		get {
			if(_collider == null) {
				_collider = GetComponent<BoxCollider2D>();
			}
			return _collider;
		}
	}
	public Dictionary<int,MachineComponent> UsedSide = new Dictionary<int, MachineComponent>();

	private float _work = 0.0f;
	public float Work
	{
		get { return _work; }
		set {
			_work = value;

			// update view(normally don't do this here...)
			UpdateBarView();
		}
	}

	protected KeyCode _key;
	public KeyCode Key1 { get { return _key; } }

	protected BarUIScript _barView;

	public abstract void Reset();
	public abstract bool Working();

	public virtual void SetupKeys()
	{
		_key = InputKeyManager.Instance.getKey();
		ControlKeyScript ctrlKey = gameObject.GetComponentInChildren<ControlKeyScript>();
		if (ctrlKey != null) {
			ctrlKey.Key = _key;
		}
	}

	protected virtual void Awake()
	{
		SetupKeys();
		_barView = gameObject.GetComponentInChildren<BarUIScript>();
		_collider = GetComponent<BoxCollider2D>();

		// Hook up event to productivity manager
		ProductivityManagerScript prodManager = GameObject.FindObjectOfType<ProductivityManagerScript>();
		if(prodManager != null)
			DoneWorkEvent += prodManager.OnProductivityDone;

		UpdateBarView();
	}

	protected virtual void Update()
	{
		if (Work >= _workGoal)
		{
			Work = 0;
			DoneWorkEvent(this, _productivity);
		}
	}

	private void UpdateBarView()
	{
		_barView?.UpdateView(_work, _workGoal);
	}
}
