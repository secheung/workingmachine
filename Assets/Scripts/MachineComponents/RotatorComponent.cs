﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorComponent : MachineComponent
{
	[SerializeField]
	private GameObject _rotationArm = null;
	[SerializeField]
	private GameObject _rotationIndicatorArm = null;

	[SerializeField]
	private float _spinRate = 1.0f;

	[SerializeField]
	private float _workRate = 1.0f;

	private bool _workClockWise = true;
	public bool WorkClockWise { get => _workClockWise; }

	public override void Reset()
	{
		_workClockWise = Random.Range(0.0f, 1.0f) > 0.5f;

		if (_rotationIndicatorArm == null) { Debug.LogWarning("No Rotator Indicator Arm Assigned"); return; }
		_rotationIndicatorArm.transform.localScale = _workClockWise ? new Vector3(-1,1,1) : new Vector3(1, 1, 1);
	}

	public override bool Working()
	{
		bool keyPress = Input.GetKey(_key);
		return (keyPress && _workClockWise) || (!keyPress && !_workClockWise);
	}

	// Update is called once per frame
	protected override void Update()
    {
		// Safety check
		if (_rotationArm == null) { Debug.LogWarning("No Rotator Arm Assigned"); return; }
		if (_rotationIndicatorArm == null) { Debug.LogWarning("No Rotator Indicator Arm Assigned"); return; }

		// Rotate by key press
		bool keyPress = Input.GetKey(_key);
		_rotationArm.transform.Rotate(-Vector3.up, (keyPress ? _spinRate: -_spinRate) * Time.deltaTime);

		// Increment work by rate
		if (Working()) {
			Work += _workRate * Time.deltaTime;
		}

		base.Update();
    }
}
