﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchComponent : MachineComponent
{
	protected Animator _animController;
	protected bool _flipped;
	protected int _flippedHash;
	[SerializeField]
	protected float _workInc = 1.0f;
	protected float _decWorkInc = 0.15f;
	protected float _minInc = 0.5f;

	// Start is called before the first frame update
	protected override void Awake()
    {
		base.Awake();
		_animController = GetComponentInChildren<Animator>();
		_flippedHash = Animator.StringToHash("flipped");
		_flipped = false;
	}

	// Update is called once per frame
	protected override void Update()
    {
		// flip switch
		if (Input.GetKeyDown(_key)) {
			_flipped = !_flipped;
		}

		// Update Work
		if (Working())
		{
			IncrementWork();
		}

		// Update Anim
		_animController?.SetBool(_flippedHash,_flipped);

		// Custom check work done
		CheckWorkDone();
	}

	protected virtual void IncrementWork()
	{
		Work = Work + _workInc;
	}

	public override void Reset()
	{
		Work = 0;
	}

	protected virtual void CheckWorkDone()
	{
		if (Work >= _workGoal)
		{
			Work = 0;

			_workInc -= _decWorkInc;
			_workInc = Mathf.Max(_minInc, _workInc);
			DoneWorkEvent(this, _productivity);
		}
	}

	public override bool Working()
	{
		return Input.GetKeyDown(_key);
	}
}
