﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterBulletComponent : MonoBehaviour
{
	float _speed = 1.0f;
	public float Speed
	{
		get { return _speed; }
		set { _speed = value; }
	}

	[SerializeField]
	BoxCollider2D _goalCollider;
	float _topLimit = 1.0f;

	Rigidbody2D _rigid;

	private void Start()
	{
		_rigid = GetComponent<Rigidbody2D>();

		gameObject.SetActive(false);
	}

	private void OnEnable()
	{
		_topLimit = _goalCollider.bounds.max.y;
	}

	// Update is called once per frame
	void Update()
    {
		_rigid.MovePosition(transform.position + Vector3.up * Time.deltaTime * _speed);
		if(transform.position.y >= _topLimit)
		{
			gameObject.SetActive(false);
		}

	}
}
