﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterComponent : MachineComponent
{
	[SerializeField]
	Transform _endPtL;
	[SerializeField]
	Transform _endPtR;
	[SerializeField]
	GameObject _ship;
	[SerializeField]
	ShooterBulletComponent _bullet;

	[SerializeField]
	float _moveSpeed = 1.0f;

	[SerializeField]
	float _bulletSpeed = 1.0f;

	float _fireTime = 0.0f;
	[SerializeField]
	float _delayFireTime = 3.0f;

	float _interpT;
	bool _movingLtoR;

	// Awake is called before the first frame update
	protected override void Awake()
    {
		base.Awake();

		_interpT = 0.5f;
		_movingLtoR = true;

		_bullet.Speed = _bulletSpeed;
	}

    // Update is called once per frame
    protected override void Update()
	{
		MoveShip();
		UpdateShoot();

		base.Update();
	}

	private void UpdateShoot()
	{
		if (_fireTime <= 0)
		{
			if (Input.GetKeyDown(_key))
			{
				//Instantiate(_bullet, _ship.transform.position, Quaternion.identity);
				_bullet.gameObject.transform.position = _ship.transform.position;
				_bullet.gameObject.SetActive(true);
				_fireTime = _delayFireTime;
			}
		}
		else
		{
			_fireTime -= Time.deltaTime;
			_fireTime = Mathf.Clamp(_fireTime, 0.0f, _delayFireTime);
		}
	}

	private void MoveShip()
	{
		Vector3 startPos = _endPtL.position;
		Vector3 endPos = _endPtR.position;

		if (_interpT >= 1.0f)
		{
			_movingLtoR = false;
		}
		else if (_interpT <= 0.0f)
		{
			_movingLtoR = true;
		}

		_interpT += (_movingLtoR ? _moveSpeed : -_moveSpeed) * Time.deltaTime;
		_interpT = Mathf.Clamp01(_interpT);
		_ship.transform.position = Vector3.Lerp(startPos, endPos, _interpT);
	}

	public override void Reset()
	{
		Work = 0;
	}

	public override bool Working()
	{
		return false;
	}

	public void OnGoal()
	{
		Work += 1;
		_bullet.gameObject.SetActive(false);
		_fireTime = 0;
	}
}
