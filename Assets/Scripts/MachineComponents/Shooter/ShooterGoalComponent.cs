﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterGoalComponent : MonoBehaviour
{
	public Action Goal;

    // Start is called before the first frame update
    void Start()
    {
		ShooterComponent sc = GetComponentInParent<ShooterComponent>();
		Goal += sc.OnGoal;
    }

	private void OnTriggerEnter2D(Collider2D other)
	{
		Goal();
	}
}
