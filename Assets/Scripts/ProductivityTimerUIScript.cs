﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductivityTimerUIScript : MonoBehaviour
{
	Image _fill;

	private float _normalizedValue;
	public float NormalizedValue
	{
		set { _normalizedValue = value; }
		get { return _normalizedValue; }
	}

    // Start is called before the first frame update
    void Start()
    {
		//GameObject circle = transform.Find("Fill").gameObject;
		_fill = transform.GetComponent<Image>();
	}

    // Update is called once per frame
    void Update()
    {
		if (_fill != null) {
			_fill.fillAmount = _normalizedValue;
		}
    }
}
