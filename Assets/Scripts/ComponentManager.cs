﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponentManager : MonoBehaviour
{
	[SerializeField]
	float _orthoSizeIncrease = 0.0f;

	[SerializeField]
	MachineComponent[] _spawnableComponents;

	Vector2 _spawnGridSize = Vector2.zero;
	HashSet<Vector2Int> _availableSpawnPts;
	HashSet<Vector2Int> _unavailableSpawnPts;

	// Start is called before the first frame update
	void Start()
	{
		SetupSpawnGrid();
		SpawnComponent();
	}

	private void SetupSpawnGrid()
	{
		// setup grid size
		foreach (MachineComponent spawnComp in _spawnableComponents)
		{
			_spawnGridSize.x = Mathf.Max(spawnComp.Collider.size.x, _spawnGridSize.x);
			_spawnGridSize.y = Mathf.Max(spawnComp.Collider.size.y, _spawnGridSize.y);
		}

		_unavailableSpawnPts = new HashSet<Vector2Int>();
		_availableSpawnPts = new HashSet<Vector2Int>();
		_availableSpawnPts.Add(Vector2Int.zero);
	}

	// Update is called once per frame
	void Update()
    {
        
    }

	MachineComponent rootComponent = null;
	public void SpawnComponent()
	{
		Camera cam = Camera.main;
		float halfheight = cam.orthographicSize;//Camera's half size when in ortho graphic mode
		float halfwidth = halfheight * cam.aspect;
		float topOffset = halfheight*2*0.05f;

		MachineComponent[] machineComps = GameObject.FindObjectsOfType<MachineComponent>();
		Collider2D[] results = new Collider2D[machineComps.Length];
		int compIndx = Random.Range(0, _spawnableComponents.Length);
		MachineComponent spawnComp = Instantiate(_spawnableComponents[compIndx], new Vector3(0, 0, 0f), Quaternion.identity, this.transform);
		Collider2D collider = spawnComp.Collider;


		SpawnInGrid(spawnComp);

		int margin = 10;
		if (collider.transform.position.x + collider.bounds.extents.x >  halfwidth              ||
			collider.transform.position.y + collider.bounds.extents.y >  halfheight - topOffset ||
			collider.transform.position.x - collider.bounds.extents.x < -halfwidth              ||
			collider.transform.position.y - collider.bounds.extents.y < -halfheight + topOffset )
		{
			cam.orthographicSize += _orthoSizeIncrease;
			halfheight = cam.orthographicSize;
			halfwidth = halfheight * cam.aspect;
		}

	}

	private void spawnAroundObj(MachineComponent spawnComp, 
								MachineComponent spawnAround,
								int pickedDirection,
								ref Collider2D[] results)
	{
		Bounds machineBounds = spawnComp.Collider.bounds;
		Bounds spawnBounds   = spawnAround.Collider.bounds;

		int side = pickedDirection;
		Vector2 newPos = Vector2.zero;
		Vector2 offset = Vector2.zero;
		switch (side)
		{
			case 1://left
				newPos.x = spawnBounds.min.x;
				newPos.y = newPos.y = Random.Range(spawnBounds.min.y, spawnBounds.max.y);//spawnBounds.center.y;
				offset.x = -machineBounds.extents.x;
				break;
			case 2://up
				newPos.x = Random.Range(spawnBounds.min.x, spawnBounds.max.x); //spawnBounds.center.x; 
				newPos.y = spawnBounds.max.y;
				offset.y = machineBounds.extents.y;
				break;
			case 3://right
				newPos.x = spawnBounds.max.x;
				newPos.y = Random.Range(spawnBounds.min.y, spawnBounds.max.y); //spawnBounds.center.y;
				offset.x = machineBounds.extents.x;
				break;
			case 4://down
				newPos.x = Random.Range(spawnBounds.min.x, spawnBounds.max.x); //spawnBounds.center.x;
				newPos.y = spawnBounds.min.y;
				offset.y = -machineBounds.extents.y;
				break;
		}

		spawnComp.transform.position = newPos + offset;
		ContactFilter2D contactFilter = new ContactFilter2D();
		contactFilter.NoFilter();
		if (spawnComp.Collider.OverlapCollider(contactFilter, results) > 0)
		{
			for(int i = 0; i < results.Length; ++i)
			{
				Collider2D result = results[i];
				if(result.transform != spawnComp.transform && result.transform != spawnAround.transform) {
					MachineComponent newBounds = result.gameObject.GetComponent<MachineComponent>();
					spawnAroundObj(spawnComp, newBounds, pickedDirection, ref results);
					break;
				}
			}
		}
		spawnAround.UsedSide.Add(side, spawnComp);
		spawnComp.UsedSide.Add((side+2)%4, spawnAround);//add the opposite side to spawned object
		spawnComp.transform.SetParent(spawnAround.transform);
	}

	private void GetRandomXY(float halfheight, float halfwidth, MachineComponent spawnComp)
	{
		// Set new Position
		float x = Random.Range(-halfwidth, halfwidth);
		float y = Random.Range(-halfheight, halfheight);
		spawnComp.transform.position = new Vector3(x, y, 0);
	}

	private void SpawnInGrid(MachineComponent spawnComp)
	{

		// Get Random element in available points
		int spawnIndx = Random.Range(0, _availableSpawnPts.Count);
		HashSet<Vector2Int>.Enumerator iter = _availableSpawnPts.GetEnumerator();
		while(spawnIndx >= 0) {
			iter.MoveNext();
			spawnIndx--;
		}

		Vector2Int spawnPt = iter.Current;

		spawnComp.transform.position = new Vector3(spawnPt.x *_spawnGridSize.x, spawnPt.y * _spawnGridSize.y, 0);

		// Remove current spawn point
		_availableSpawnPts.Remove(spawnPt);
		_unavailableSpawnPts.Add(spawnPt);

		Vector2Int testPt = Vector2Int.zero;
		// Add available points surrounding grid(if already don't add)
		testPt = spawnPt + Vector2Int.up                     ; if(!_unavailableSpawnPts.Contains(testPt)){ _availableSpawnPts.Add(testPt); }// top
		//testPt = spawnPt + Vector2Int.up + Vector2Int.right  ; if(!_unavailableSpawnPts.Contains(testPt)){ _availableSpawnPts.Add(testPt); }// top right
		testPt = spawnPt + Vector2Int.right                  ; if(!_unavailableSpawnPts.Contains(testPt)){ _availableSpawnPts.Add(testPt); }// right
		//testPt = spawnPt + Vector2Int.down + Vector2Int.right; if(!_unavailableSpawnPts.Contains(testPt)){ _availableSpawnPts.Add(testPt); }// down right
		testPt = spawnPt + Vector2Int.down                   ; if(!_unavailableSpawnPts.Contains(testPt)){ _availableSpawnPts.Add(testPt); }// down
		//testPt = spawnPt + Vector2Int.down + Vector2Int.left ; if(!_unavailableSpawnPts.Contains(testPt)){ _availableSpawnPts.Add(testPt); }// down left
		testPt = spawnPt + Vector2Int.left                   ; if(!_unavailableSpawnPts.Contains(testPt)){ _availableSpawnPts.Add(testPt); }// left
		//testPt = spawnPt + Vector2Int.up + Vector2Int.left   ; if(!_unavailableSpawnPts.Contains(testPt)){ _availableSpawnPts.Add(testPt); }// top left
	}
}
