﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductivityManagerScript : MonoBehaviour
{
	protected BarUIScript _barView;

	[SerializeField]
	float _productivityGoal;
	public float ProductivityGoal
	{
		get { return _productivityGoal; }
		set { _productivityGoal = value; }
	}

	[SerializeField]
	float _productivity;
	public float Productivity { get => _productivity; }

	public static int _score; // shitty method for saving score between scenes but eh fuck it it's almost the deadline and I'm tired.

	public Action NextProductivityGoalEvent = delegate { };

    // Start is called before the first frame update
    void Start()
    {
		_score = 0;
		_barView = GetComponentInChildren<BarUIScript>();

		_productivity = 0.0f;
		_productivityGoal = 2.0f;
		UpdateBarView();
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	public void OnProductivityDone(object sender, float productivity)
	{
		_productivity += productivity;
		if(_productivity >= _productivityGoal) {
			_productivity = 0;
			_score++;

			NextProductivityGoalEvent();
		}
		UpdateBarView();
	}

	private void UpdateBarView()
	{
		_barView?.UpdateView(_productivity, _productivityGoal);
	}
}
