﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class TitleTransitions : MonoBehaviour
{
	[SerializeField]
	KeyCode _key = KeyCode.Space;
	[SerializeField]
	string _scene = "";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(_key))
		{
			SceneManager.LoadScene(_scene);
		}
    }
}
