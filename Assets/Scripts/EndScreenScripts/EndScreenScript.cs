﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndScreenScript : MonoBehaviour
{
	float _finalProductivity;

	[SerializeField]
	TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
		text.text = ProductivityManagerScript._score.ToString();

	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
