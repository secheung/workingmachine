﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputKeyManager
{
	private static InputKeyManager _instance;
	public static InputKeyManager Instance
	{
		get
		{
			if (_instance == null) {
				_instance = new InputKeyManager();
			}

			return _instance;
		}
	}

	private HashSet<KeyCode> _usedKeys = new HashSet<KeyCode>();

	public KeyCode getKey() {
		KeyCode key = 0;
		do
		{
			int index = Random.Range(97, 123);//A - Z
			key = (KeyCode)index;
		} while (_usedKeys.Contains(key));

		_usedKeys.Add(key);

		// if we run out of keys double up on controls.... It'll add to the fun!
		if (_usedKeys.Count >= 26) {
			_usedKeys.Clear();
		}

		return key;
	}
}
