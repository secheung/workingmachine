﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyManagerScript : MonoBehaviour
{
	[SerializeField]
	ProductivityManagerScript _productManager;
	[SerializeField]
	ProductivityTimerScript _productTimer;
	[SerializeField]
	ComponentManager _compManager;

	[SerializeField]
	float _decreaseTime;
	[SerializeField]
	float _increaseGoal;
	[SerializeField]
	float _spawnChance = 0.25f;

	private float Productivity
	{
		get => _productManager.ProductivityGoal;
	}

    // Start is called before the first frame update
    void Start()
    {
		_productManager.NextProductivityGoalEvent += OnProductivityDone;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	void OnProductivityDone()
	{
		int selection = Random.Range(1, 4);
		switch (selection)
		{
			case 1:
				IncreaseProductivityGoal();
				break;
			case 2:
				DecreaseTime();
				break;
		}

		IncreaseComponents();
	}

	void IncreaseComponents()
	{
		int score = ProductivityManagerScript._score;

		if (score > 15) // Spawn everytime in late game
		{
			_compManager.SpawnComponent();
		}
		else if (score > 5)
		{
			if (score % 2 == 0)
			{
				_compManager.SpawnComponent();
			}
		}
		else if(score > 2) // Spawn once every 3 goals during late game
		{
			if (score % 3 == 0)
			{
				_compManager.SpawnComponent();
			}
		}
		else if (score > 1) // Don't spawn while still early
		{
			float spawnChance = Random.Range(0.0f, 1.0f);
			if (spawnChance < _spawnChance)
			{
				_compManager.SpawnComponent();
			}
			else
			{
				_spawnChance += 0.25f;
				_spawnChance = Mathf.Min(_spawnChance, 1.0f);
			}
		}
	}

	void IncreaseProductivityGoal()
	{
		_productManager.ProductivityGoal += _increaseGoal;
	}

	void DecreaseTime()
	{
		_productTimer.TotalTime -= _decreaseTime;
		_productTimer.TotalTime = Mathf.Max(_productTimer.TotalTime, 5.0f);
	}


}
