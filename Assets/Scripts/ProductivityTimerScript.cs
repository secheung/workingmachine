﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductivityTimerScript : MonoBehaviour
{
	public Action TimeUpEvent = delegate { };
	ProductivityTimerUIScript _timerUI;

	[SerializeField]
	float _totalTime = 1.0f;
	public float TotalTime
	{
		get { return _totalTime; }
		set { _totalTime = value; }
	}

	float _currentTime;


    // Start is called before the first frame update
    void Start()
    {
		_timerUI = GetComponentInChildren<ProductivityTimerUIScript>();
		_currentTime = _totalTime;

		// Hook up event to Timer manager
		ProductivityManagerScript productivityManager = GameObject.FindObjectOfType<ProductivityManagerScript>();
		productivityManager.NextProductivityGoalEvent += OnProductivityDone;
	}

    // Update is called once per frame
    void Update()
    {
		_currentTime -= Time.deltaTime;
		if(_currentTime <= 0) {
			_currentTime = 0;
			TimeUpEvent();
		}

		if(_timerUI != null) {
			_timerUI.NormalizedValue = _currentTime / _totalTime;
		}
    }

	public void OnProductivityDone()
	{
		_currentTime = _totalTime;
	}
}
