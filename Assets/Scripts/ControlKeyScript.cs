﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ControlKeyScript : MonoBehaviour
{
	private KeyCode _key;
	public KeyCode Key
	{
		get { return _key; }
		set {
			_key = value;
			UpdateKeyDisplay();
		}
	}

	TextMeshPro _text = null;

    // Start is called before the first frame update
    void Start()
    {
		_text = GetComponentInChildren<TextMeshPro>();
		//MachineComponent machineComp = GetComponentInParent<MachineComponent>();
		//if (machineComp != null){
		//	_key = machineComp.Key1;
		//}

		UpdateKeyDisplay();
    }

	void UpdateKeyDisplay()
	{
		_text?.SetText(_key.ToString());
	}
}
